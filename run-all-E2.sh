#!/bin/bash

echo "==="
echo "E2: Strong-ZKP"
echo "==="
echo ""

echo "==="
echo "Crypto-Santa_Anonymous-Shuffling_Strong-ZKP.pv:"
echo "==="
time ./proverif2.05/proverif Crypto-Santa_Anonymous-Shuffling_Strong-ZKP.pv > Crypto-Santa_Anonymous-Shuffling_Strong-ZKP.pv.out
grep -A 4 -B 1 "Verification summary" Crypto-Santa_Anonymous-Shuffling_Strong-ZKP.pv.out

echo "==="
echo "Exp-MixNets_Anonymous-Shuffling_Strong-ZKP.pv:"
echo "==="
time ./proverif2.05/proverif Exp-MixNets_Anonymous-Shuffling_Strong-ZKP.pv > Exp-MixNets_Anonymous-Shuffling_Strong-ZKP.pv.out		I
grep -A 4 -B 1 "Verification summary" Exp-MixNets_Anonymous-Shuffling_Strong-ZKP.pv.out	

echo "==="
echo "Haenni-Protocol_Vote-Privacy_Strong-ZKP.pv:"
echo "==="
time ./proverif2.05/proverif Haenni-Protocol_Vote-Privacy_Strong-ZKP.pv	> Haenni-Protocol_Vote-Privacy_Strong-ZKP.pv.out
grep -A 4 -B 1 "Verification summary" Haenni-Protocol_Vote-Privacy_Strong-ZKP.pv.out

echo "==="
echo "IVXV-Protocol_Vote-Privacy_Strong-ZKP.pv:"
echo "==="
time ./proverif2.05/proverif IVXV-Protocol_Vote-Privacy_Strong-ZKP.pv > IVXV-Protocol_Vote-Privacy_Strong-ZKP.pv.out
grep -A 4 -B 1 "Verification summary" IVXV-Protocol_Vote-Privacy_Strong-ZKP.pv.out

echo "==="
echo "Remark_Anonymous-Marking_Strong-ZKP.pv:"
echo "==="
time ./proverif2.05/proverif Remark_Anonymous-Marking_Strong-ZKP.pv > Remark_Anonymous-Marking_Strong-ZKP.pv.out
grep -A 4 -B 1 "Verification summary" Remark_Anonymous-Marking_Strong-ZKP.pv.out

echo "==="
echo "ReEncryption-MixNets_Anonymous-Shuffling_Strong-ZKP.pv:"
echo "==="
time ./proverif2.05/proverif ReEncryption-MixNets_Anonymous-Shuffling_Strong-ZKP.pv > ReEncryption-MixNets_Anonymous-Shuffling_Strong-ZKP.pv.out
grep -A 4 -B 1 "Verification summary" ReEncryption-MixNets_Anonymous-Shuffling_Strong-ZKP.pv.out
