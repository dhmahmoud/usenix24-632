#!/bin/bash

## you will need to enter your root password during this script execution
## at some points, you will be prompted to choose options: always press ENTER (this will select default option)

## dependencies
sudo apt install time libgtk2.0-dev opam curl graphviz build-essential wget -y
opam init
opam update
eval $(opam env --switch=default)
opam install lablgtk
eval $(opam config env)

## proverif
wget https://bblanche.gitlabpages.inria.fr/proverif/proverif2.05.tar.gz
tar -xzf proverif2.05.tar.gz
cd proverif2.05
./build
