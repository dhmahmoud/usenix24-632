(* Protocol: Exponentiation MixNets *)
(* Property: Anonymous Shuffling *)
(* With  Weak Zero knowledge Proofs *)
(* Result: attack *)

set simplifyProcess = false.
set reconstructTrace = false.

(* Secrecy assumptions *)
not x:channel; attacker(diff[new chMix1,x]).
not x:channel; attacker(diff[x,new chMix1]).
not x:channel; attacker(diff[new chMix2,x]).
not x:channel; attacker(diff[x,new chMix2]).

select x,x':bitstring; mess(new chMix1, diff[*x,*x']).
select x,x':bitstring; mess(new chMix2, diff[*x,*x']).

not x:exponent; attacker(diff[new skN,x]).
not x:exponent; attacker(diff[x,new skN]).
not x:exponent; attacker(diff[new skS1,x]).
not x:exponent; attacker(diff[x,new skS1]).
not x:exponent; attacker(diff[new skS2,x]).
not x:exponent; attacker(diff[x,new skS2]).


(* Main channel *)
free ch: channel.

(* Types declaration *)
type G.
type exponent.

(* Constants / Free names *)
const g:G [data].

(* Equational theory *)
fun exp(G, exponent): G.
equation forall x: exponent, y: exponent, z: exponent; exp(exp(exp(g, x), y), z) = exp(exp(exp(g, x), z), y).
equation forall x: exponent, y: exponent; exp(exp(g, x), y) = exp(exp(g, y), x).

fun h(G): bitstring.
(* weal zkp of possessing secret key *)
fun WZKP(G, G, exponent): bitstring.
reduc forall x: exponent, X: G, A:G; zkpcheck(WZKP(A, X, x), X, exp(X, x), h(A)) = true.


(*---------------Protocol-----------------*)

(*----------Mix-NET-----------------*)
let Mixnet(skN: exponent, mix1: channel, mix2: channel) =
  (* we use dedicated channels for the two keys to be swapped to ensure they are both mixed *)
  in(mix1, (pkX1: G, pkX1zkp: bitstring, A1: bitstring));
  in(mix2, (pkX2: G, pkX2zkp: bitstring, A2: bitstring));
  (* let the adversary also choose a key to mix and an associated ZKP *)
  in(ch, (pkX3: G, pkX3zkp: bitstring, A3: bitstring, X: G));
  (* but they are not allowed to simply copy an existing key! *)
  if(pkX3 <> pkX1 && pkX3 <> pkX2) then
  if (zkpcheck(pkX1zkp, g, pkX1, A1) = true) then
  if (zkpcheck(pkX2zkp, g, pkX2, A2) = true) then
  if (zkpcheck(pkX3zkp, X, pkX3, A3) = true) then
  (
  out(ch, choice[exp(pkX1, skN), exp(pkX2, skN)]) |
  out(ch, choice[exp(pkX2, skN), exp(pkX1, skN)]) |
  out(ch, exp(pkX3, skN))).

(*---------Honest Sender ---------*)
let Sender(skS: exponent, chmix: channel) =
  new a: exponent; (** Secret key for the commitement **)
  let A = exp(g, a) in
  out(chmix, (exp(g, skS), WZKP(A, g, skS), h(A)));
  (* also publicly send message *)
  out(ch, (exp(g, skS), WZKP(A, g, skS), h(A))).


(*---------------------Main-----------------*)
process
  (** Mix-Nets' key generation **)
  new skN: exponent; let pkN = exp(g, skN) in out (ch, pkN);
  (** Honest Senders' keys generation **)
  new chMix1:channel; new skS1: exponent; let pkS1 = exp(g, skS1) in
  new chMix2:channel; new skS2: exponent; let pkS2 = exp(g, skS2) in
  (** Protocol's execution **)
  (!Mixnet(skN, chMix1, chMix2)) | Sender(choice[skS2, skS1], chMix1) | Sender(choice[skS1, skS2], chMix2)
