(* Remark! protocol *)
(* Property: Anonymous Examiner *)
(* Strong Zero knowledge Proofs *)
(* Result: Proof *)

set simplifyProcess = false.
set reconstructTrace = false.


not x:channel; attacker(diff[new chMix1,x]).
not x:channel; attacker(diff[x,new chMix1]).
not x:channel; attacker(diff[new chMix2,x]).
not x:channel; attacker(diff[x,new chMix2]).

select x,x':bitstring; mess(new chMix1, diff[*x,*x']).
select x,x':bitstring; mess(new chMix2, diff[*x,*x']).

not x:exponent; attacker(diff[new skN,x]).
not x:exponent; attacker(diff[x,new skN]).
not x:exponent; attacker(diff[new skE1,x]).
not x:exponent; attacker(diff[x,new skE1]).
not x:exponent; attacker(diff[new skE2,x]).
not x:exponent; attacker(diff[x,new skE2]).
not x:exponent; attacker(diff[new re,x]).
not x:exponent; attacker(diff[x,new re]).

const ch_Exam_Marking:channel [private].

not attacker(ch_Exam_Marking).

not x:channel; attacker(diff[ch_Exam_Marking,x]).
not x:channel; attacker(diff[x,ch_Exam_Marking]).

select x,x':bitstring; mess(ch_Exam_Marking,  diff[*x,*x']).




free ch: channel.
(*Public channels representing the bulletin-board*)
free bb: channel.

(* Types declaration *)
type G.
type exponent.
type coins.
type role.

(* Constants / Free names *)
free mark1, mark2:bitstring.
const g:G [data].

(*Roles of the Candidates and Examiners*)
free C: role.
free E: role.

(* Pseudonym *)
fun exp(G, exponent): G.

equation forall x: exponent, y: exponent, z: exponent; exp(exp(exp(g, x), y), z) = exp(exp(exp(g, x), z), y).
equation forall x: exponent, y: exponent; exp(exp(g, x), y) = exp(exp(g, y), x).

(* El-Gamal Probabilistic Public key encryption *)
fun encrypt(bitstring, G, G, coins): bitstring.
reduc forall m: bitstring, X: G, y: exponent, r: coins;
  decrypt(encrypt(m, X, exp(X, y), r), X, y) = m.

(* Signature *)
fun sign(bitstring, G, exponent): bitstring.
reduc forall m: bitstring, X: G, s: exponent; getmess(sign(m, X, s)) = m.
reduc forall m: bitstring, X: G, s: exponent; checksign(sign(m, X, s), X, exp(X, s)) = m.

fun exponentTobitstring(exponent): bitstring [typeConverter].

fun h(G, G, G): bitstring.
(* strong zkp of possessing the secret key *)
fun zkp(G, G, exponent): bitstring.
reduc forall x: exponent, X: G, A:G; zkpcheck(zkp(A, g, x), g, exp(g, x), h(g, exp(g, x), A)) = true.
(*---------------Protocol-----------------*)


(*----------Mix-Nets-----------------*)
let NET(skN:exponent, re: exponent, chMix1: channel, chMix2: channel) =
(* we use dedicated channels for the two keys to be swapped to ensure they are both mixed *)
in(chMix1, (=E, pkX1: G, pkX1zkp: bitstring, A1: bitstring));
in(chMix2, (=E, pkX2: G, pkX2zkp: bitstring, A2: bitstring));
(* let the dishonest examiner choose a key to mix and an associated zkp *)
in(ch, (=E, pkX3: G, pkX3zkp: bitstring, A3: bitstring));
(* but they are not allowed to choose an existing key *)
if (pkX3 <> pkX1) && (pkX3 <> pkX2) then
let hx = exp(g, re) in
if (zkpcheck(pkX1zkp, g, pkX1, A1) = true) then
let pseudo_X1 = exp(pkX1,re) in
let spseX1 = sign ((pseudo_X1, hx, E), g, skN) in

if (zkpcheck(pkX2zkp, g, pkX2, A2) = true) then
let pseudo_X2 = exp(pkX2,re) in
let spseX2 = sign ((pseudo_X2, hx, E), g, skN) in

if (zkpcheck(pkX3zkp, g, pkX3, A3) = true) then
let pseudo_X3 = exp(pkX3,re) in
let spseX3 = sign ((pseudo_X3, hx, E), g, skN) in

(*Pseudonyms of the two honest Examiners are published in random order.*)
(
out(bb, choice[(pseudo_X1, hx, E, spseX1), (pseudo_X2, hx, E, spseX2)]) |
out(bb, choice[(pseudo_X2, hx, E, spseX2), (pseudo_X1, hx, E, spseX1)]) |
out(bb, (pseudo_X3, hx, E, spseX3)) ).

(*----------Examiner-----------------*)

(******* Process of Examiner during marking phase *******)
let Examiner_Marking =
  !
  in(ch_Exam_Marking,(skE:exponent, pkA:G, mark:bitstring, he:G, =C));
  in(bb, eca'':bitstring);
  let ((ques:bitstring, ans:bitstring, pseudo_C:G, =C), sca':bitstring) = decrypt(eca'', he, skE) in
  if (ques, ans, pseudo_C, C) = checksign(sca', g, pkA) then
  let ca = (ques, ans, pseudo_C) in
  let ma:bitstring = (ca, sca', mark) in
  let sma:bitstring =  sign(ma,he,skE) in
  new r5[]: coins;
  let ema = encrypt((ma, sma), g, pkA, r5) in
  out(ch, ema)
.

(******* Process of Examiner during registration phase *******)
let Examiner (skE:exponent, pkA:G, pkN:G, mark:bitstring, rl:role) =
  in(bb, (pseudo_E:G, he:G, r: role, spseE:bitstring));
  if (pseudo_E, he, rl) = checksign(spseE, g, pkN) && rl = r then
  let priv_E = exp(he, skE) in
  if pseudo_E = priv_E then

  out(ch_Exam_Marking,(skE, pkA, mark, he, rl))
.


(*----------Main-----------------*)
process

(* Exponent of the Mix-Nets *)
new re: exponent;

(*We assume one NET and one Exam Authority*)
new skA: exponent; let pkA = exp(g, skA) in out (ch, (pkA, skA)); (*The secret key of the Exam Authority is given to the attacker*)
new skN: exponent; let pkN = exp(g, skN) in out (ch, pkN);

(*Keys of the two honest Examiners*)
new chMix1: channel; new skE1: exponent; let pkE1 = exp(g, skE1) in new a1: exponent; let A1 = exp(g, a1) in
new chMix2: channel; new skE2: exponent; let pkE2 = exp(g, skE2) in new a2: exponent; let A2 = exp(g, a2) in

out(ch, pkE1); out(chMix1, (E, pkE1, zkp(A1, g, skE1), h(g, pkE1, A1))); out(ch, (E, pkE1, zkp(A1, g, skE1), h(g, pkE1, A1)));
out(ch, pkE2); out(chMix2, (E, pkE2, zkp(A2, g, skE2), h(g, pkE2, A2))); out(ch, (E, pkE2, zkp(A2, g, skE2), h(g, pkE2, A2)));

(* Honest Parties *)
(NET(skN, re, chMix1, chMix2)) |

(* We swap keys rather than marks  we*)
 (Examiner(choice[skE1, skE2], pkA, pkN, mark1, E)) |
 (Examiner(choice[skE2, skE1], pkA, pkN, mark2, E)) | (Examiner_Marking)
