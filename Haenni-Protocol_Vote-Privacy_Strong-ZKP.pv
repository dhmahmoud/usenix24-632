(* Haenni Voting protocol *)
(* Property: Vote Privacy *)
(* Strong Zero knowledge Proofs *)
(* Result: Proof *)


set simplifyProcess = false.
set reconstructTrace = false.

(* Secrecy assumptions *)
not x:channel; attacker(diff[new chMix1,x]).
not x:channel; attacker(diff[x,new chMix1]).
not x:channel; attacker(diff[new chMix2,x]).
not x:channel; attacker(diff[x,new chMix2]).

select x,x':bitstring; mess(new chMix1, diff[*x,*x']).
select x,x':bitstring; mess(new chMix2, diff[*x,*x']).

not x:exponent; attacker(diff[new skN,x]).
not x:exponent; attacker(diff[x,new skN]).
not x:exponent; attacker(diff[new skV1,x]).
not x:exponent; attacker(diff[x,new skV1]).
not x:exponent; attacker(diff[new skV2,x]).
not x:exponent; attacker(diff[x,new skV2]).
not x:exponent; attacker(diff[new a1,x]).
not x:exponent; attacker(diff[x,new a1]).
not x:exponent; attacker(diff[new a2,x]).
not x:exponent; attacker(diff[x,new a2]).
not x:exponent; attacker(diff[new rv,x]).
not x:exponent; attacker(diff[x,new rv]).

free ch: channel.
(* Public channel representing the bulletin-board *)
free bb: channel.

(* candidates *)
free cand1:bitstring.
free cand2:bitstring.

(* Data types *)
type G. (* the DH group *)
type exponent. (* the exponents *)
const g: G [data]. (* the generator *)
type coins.
type role.

(* roles *)
free V: role.

fun pkey_to_bitstring(G): bitstring [data, typeConverter].

(* Pseudonym *)
fun exp(G, exponent): G.
equation forall x: exponent, y: exponent, z: exponent; exp(exp(exp(g, x), y), z) = exp(exp(exp(g, x), z), y).
equation forall x: exponent, y: exponent; exp(exp(g, x), y) = exp(exp(g, y), x).

(* El-Gamal Probabilistic Public key encryption *)
fun encrypt(bitstring, G, G, coins): bitstring.
reduc forall m: bitstring, X: G, y: exponent, r: coins;
  decrypt(encrypt(m, X, exp(X, y), r), X, y) = m.

(* Signature *)
fun sign(bitstring, G, exponent): bitstring.
reduc forall m: bitstring, X: G, s: exponent; getmess(sign(m, X, s)) = m.
reduc forall m: bitstring, X: G, s: exponent; checksign(sign(m, X, s), X, exp(X, s)) = m.

fun h(G, G, G): bitstring.
(* zkp proof possession of secret key *)
fun zkp(G, G, exponent): bitstring.
reduc forall x: exponent, X: G, A:G; zkpcheck(zkp(A, g, x), g, exp(g, x), h(g, exp(g, x), A)) = true.

(*---------------Protocol-----------------*)

(*----------NET-----------------*)
let NET (skN:exponent, rv:exponent, pkA:G, chMix1: channel, chMix2: channel) =
  (* we use dedicated channels for the two keys to be swapped to ensure they are both mixed *)
  in(chMix1, (rx1: role, pkX1: G, certx1: bitstring, pkX1zkp: bitstring, A1: bitstring));
  in(chMix2, (rx2: role, pkX2: G, certx2: bitstring, pkX2zkp: bitstring, A2: bitstring));
  (* let the adversary also choose a key to mix and a corresponding zkp *)
  in(ch, (rx3: role, pkX3: G, certx3: bitstring, pkX3zkp: bitstring, A3: bitstring));
  (* but they are not allowed to simply copy an existing key! *)
  if(pkX3 <> pkX1 && pkX3 <> pkX2) then
  let hx = exp(g, rv) in
  if checksign(certx1, g, pkA) = pkey_to_bitstring(pkX1) then
  if (zkpcheck(pkX1zkp, g, pkX1, A1) = true) then
  let pseudo_X1 = exp(pkX1, rv) in
  let spseX1 = sign ((pseudo_X1, hx, V), g, skN) in

  if checksign(certx2, g, pkA) = pkey_to_bitstring(pkX2) then
  if (zkpcheck(pkX2zkp, g, pkX2, A2) = true) then
  let pseudo_X2 = exp(pkX2, rv) in
  let spseX2 = sign ((pseudo_X2, hx, V), g, skN) in

  if checksign(certx3, g, pkA) = pkey_to_bitstring(pkX3) then
  if (zkpcheck(pkX3zkp, g, pkX3, A3) = true) then
  let pseudo_X3 = exp(pkX3, rv) in
  let spseX3 = sign ((pseudo_X3, hx, V), g, skN) in

  (* Pseudonyms of the two honest voters are published in random order, so we can use a choice here to avoid a spurious attack. *)
  (
  out(bb,  choice[(pseudo_X1, hx, V, spseX1),(pseudo_X2, hx, V, spseX2)]) |
  out(bb,  choice[(pseudo_X2, hx, V, spseX2),(pseudo_X1, hx, V, spseX1)]) |
  out(bb,  (pseudo_X3, hx, V, spseX3)) | out(bb, hx)
  ).

(*----------Voters-----------------*)

let Voter (skV:exponent, pkT:G, pkN:G, cand:bitstring, rl:role) =
  (* Voter Registration *)
  in(bb, (pseudo_V:G, hv:G, r: role, spseV:bitstring));
  if (pseudo_V, hv, r) = checksign(spseV, g, pkN)  && r = rl  then
  if (exp(hv, skV) = pseudo_V) then
  (* Voting *)
  new rand : coins;
  out(ch, sign(encrypt(cand,g,pkT,rand),hv,skV)).


(*----------Main-----------------*)
process

(* exponent of the Mix-NET) *)
new rv: exponent;

(* We assume one NET, one Authority, and a Tallier *)
(* Authority and Tallier are not trusted. *)
new skA: exponent; let pkA = exp(g, skA) in out (ch, (pkA, skA));
new skN: exponent; let pkN = exp(g, skN) in out (ch, pkN);
new skT: exponent; let pkT = exp(g, skT) in out (ch, (pkT, skT));

(* Generate keys of the two honest Voters *)
(* and send them to the Mix-Net *)
new chMix1: channel; new skV1: exponent; let pkV1 = exp(g, skV1) in new a1: exponent;
let A1 = exp(g, a1) in out(ch, A1);
out(ch, (V, pkV1, sign(pkey_to_bitstring(pkV1), g, skA), zkp(A1, g, skV1), h(g, pkV1, A1)));
out(chMix1, (V, pkV1, sign(pkey_to_bitstring(pkV1), g, skA), zkp(A1, g, skV1), h(g, pkV1, A1)));

new chMix2: channel; new skV2: exponent; let pkV2 = exp(g, skV2) in new a2: exponent;
let A2 = exp(g, a2) in out(ch, A2);
out(ch, (V, pkV2, sign(pkey_to_bitstring(pkV2), g, skA), zkp(A2, g, skV2), h(g, pkV2, A2)));
out(chMix2, (V, pkV2, sign(pkey_to_bitstring(pkV2), g, skA), zkp(A2, g, skV2), h(g, pkV2, A2)));

(* Honest Parties *)
(NET(skN, rv, pkA, chMix1, chMix2)) |

(* we swap keys rather than candidates to avoid spurious attacks *)
(* this implicitely also assumes an anonymous channel for the voters *)
 ( Voter(choice[skV1,skV2], pkT, pkN, cand1, V) ) |
 ( Voter(choice[skV2,skV1], pkT, pkN, cand2, V) )
