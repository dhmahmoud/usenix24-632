(* Remark! protocol *)
(* Property: Anonymous Marking *)
(* Without Zero knowledge Proofs *)
(* Result: Attack *)

set simplifyProcess = false.
set reconstructTrace = false.


not x:channel; attacker(diff[new chMix1,x]).
not x:channel; attacker(diff[x,new chMix1]).
not x:channel; attacker(diff[new chMix2,x]).
not x:channel; attacker(diff[x,new chMix2]).

select x,x':bitstring; mess(new chMix1, diff[*x,*x']).
select x,x':bitstring; mess(new chMix2, diff[*x,*x']).

not x:exponent; attacker(diff[new skN,x]).
not x:exponent; attacker(diff[x,new skN]).
not x:exponent; attacker(diff[new skC1,x]).
not x:exponent; attacker(diff[x,new skC1]).
not x:exponent; attacker(diff[new skC2,x]).
not x:exponent; attacker(diff[x,new skC2]).
not x:exponent; attacker(diff[new rc,x]).
not x:exponent; attacker(diff[x,new rc]).

free ch: channel.
(*Public channels representing the bulletin-board*)
free bb: channel.

(* Types declaration *)
type G.
type exponent.
type coins.
type role.

(* Constants / Free names *)
const g:G [data].
free ans1:bitstring.
free ans2:bitstring.


(*Roles of Candidates and Examiners*)
free C: role.
free E: role.


(* Pseudonym *)
fun exp(G, exponent): G.
equation forall x: exponent, y: exponent, z: exponent; exp(exp(exp(g, x), y), z) = exp(exp(exp(g, x), z), y).
equation forall x: exponent, y: exponent; exp(exp(g, x), y) = exp(exp(g, y), x).

(* El-Gamal Probabilistic Public key encryption *)
fun encrypt(bitstring, G, G, coins): bitstring.
reduc forall m: bitstring, X: G, y: exponent, r: coins;
  decrypt(encrypt(m, X, exp(X, y), r), X, y) = m.

(* Signature *)
fun sign(bitstring, G, exponent): bitstring.
reduc forall m: bitstring, X: G, s: exponent; getmess(sign(m, X, s)) = m.
reduc forall m: bitstring, X: G, s: exponent; checksign(sign(m, X, s), X, exp(X, s)) = m.

fun exponentTobitstring(exponent): bitstring [typeConverter].

(*---------------Protocol-----------------*)

(*----------Mix-Nets-----------------*)
let NET(skN:exponent, re: exponent, chMix1: channel, chMix2: channel) =
(* we use dedicated channels for the two keys to be swapped to ensure they are both mixed *)
in(chMix1, (=C, pkX1: G));
in(chMix2, (=C, pkX2: G));
(* let the dishonest candidate choose a key to mix *)
in(ch, (=C, pkX3: G));
(* but they are not allowed to choose an existing key *)
if (pkX3 <> pkX1) && (pkX3 <> pkX2) then
let hx1 = exp(g, re) in
let pseudo_X1 = exp(pkX1,re) in
let spseX1 = sign ((pseudo_X1, hx1, E), g, skN) in

let hx2 = exp(g, re) in
let pseudo_X2 = exp(pkX2,re) in
let spseX2 = sign ((pseudo_X2, hx2, E), g, skN) in

let hx3 = exp(g, re) in
let pseudo_X3 = exp(pkX3,re) in
let spseX3 = sign ((pseudo_X3, hx3, E), g, skN) in

(*Pseudonyms of the two honest Candidates are published in random order.*)
(
out(bb, choice[(pseudo_X1, hx1, E, spseX1), (pseudo_X2, hx2, E, spseX2)]) |
out(bb, choice[(pseudo_X2, hx2, E, spseX2), (pseudo_X1, hx1, E, spseX1)]) |
out(bb, (pseudo_X3, hx3, E, spseX3)) ).

(*----------Candidate-----------------*)



const ch_Cand_Exam,ch_Cand_Exam_End:channel [private].

not x:channel; attacker(diff[ch_Cand_Exam,x]).
not x:channel; attacker(diff[x,ch_Cand_Exam]).
not x:channel; attacker(diff[ch_Cand_Exam_End,x]).
not x:channel; attacker(diff[x,ch_Cand_Exam_End]).

select x,x':bitstring; mess(ch_Cand_Exam, diff[*x,*x']).
select x,x':bitstring; mess(ch_Cand_Exam_End,  diff[*x,*x']).


(******* Process of Candidate at the end of examination phase *******)
let Candidate_Exam_End =
  !
  in(ch_Cand_Exam_End,(skC:exponent,pkA:G, pkN:G, ca:bitstring, hc:G, =C));

  in(bb, eca':bitstring);
  let (=ca, sca':bitstring) = decrypt(eca', hc, skC) in
  if ca = checksign(sca', g, pkA) then
  0
.

(******* Process of Candidate during examination phase *******)
let Candidate_Exam =
  !
  in(ch_Cand_Exam,(skC:exponent,pkA:G, pkN:G, ans:bitstring,hc:G,pseudo_C:G, =C));
  in(bb, eques:bitstring);
  let (ques: bitstring, sques:bitstring) = decrypt(eques, hc, skC) in
  if ques=checksign(sques, g, pkA) then
  let ca = (ques, ans, pseudo_C, C) in
  let sca = sign(ca,hc,skC) in
  new r6[]: coins;
  let eca = encrypt((ca, sca), g, pkA, r6) in
  out(ch, eca);

  out(ch_Cand_Exam_End,(skC,pkA, pkN, ca, hc, C))
.


(******* Process of Candidate during registration phase *******)
let Candidate (skC:exponent, pkA:G, pkN:G, ans:bitstring, rl:role) =

  in(bb, (pseudo_C:G, hc:G, =C, spseC:bitstring));
  if (pseudo_C, hc, rl) = checksign(spseC, g, pkN) then
  let priv_C = exp(hc, skC) in
  if priv_C = pseudo_C then

  out(ch_Cand_Exam,(skC,pkA, pkN, ans,hc,pseudo_C, C))
.



(*----------Main-----------------*)
process

(* Exponent of the Mix-Nets *)
new rc: exponent;

(*We assume one NET and one Exam Authority*)
new skA: exponent; let pkA = exp(g, skA) in out (ch, (pkA, skA)); (*The secret key of the Exam Authority is given to the attacker*)
new skN: exponent; let pkN = exp(g, skN) in out (ch, pkN);

(*Keys of the two honest Candidates*)
new chMix1: channel; new skC1: exponent; let pkC1 = exp(g, skC1) in
new chMix2: channel; new skC2: exponent; let pkC2 = exp(g, skC2) in

out(ch, pkC1); out(chMix1, (C, pkC1)); out(ch, (C, pkC1));
out(ch, pkC2); out(chMix2, (C, pkC2)); out(ch, (C, pkC2));

(* Honest Parties *)
(NET(skN, rc, chMix1, chMix2)) |

(* We swap keys rather than answers to avoid spurious attacks *)
 (Candidate(choice[skC1, skC2], pkA, pkN, ans1, C)) |
 (Candidate(choice[skC2, skC1], pkA, pkN, ans2, C)) |
 (Candidate_Exam) | (Candidate_Exam_End)
