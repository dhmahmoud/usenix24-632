# Artifacts of research paper "Shaken, not Stirred - Automated Discovery of Subtle Attacks on Protocols using Mix-Nets"[^10], co-authored by Jannik Dreier[^0], Pascal Lafourcade[^1] and Dhekra Mahmoud[^2], for USENIX 2024 Conference

This repository gathers symbolic models of protocols using Mix-Nets, using the automatic protocol verifier ProVerif[^9]. The following protocols are analyzed:

- `Crypto-Santa` [^4] is based on a Christmas tradition called Secret Santa. During this ceremony, the participants are randomly assigned a person to whom they give a gift and the identity of the gift giver should remain secret.
- `Exp-MixNets` [^5]: Servers create, from a list of ElGamal public keys, a new shuffled list of anonymized public keys, which can no longer be associated to individual parties.
- `Haenni-Protocol` [^5]: An internet vote protocol introducing the concept of exponentiation Mix-Nets.
- `IVXV-Protocol` [^6]: The Estonian internet vote protocol which is used to conduct legally binding political elections in Estonia.
- `ReEncryption-MixNets` [^7]: Both the input and output lists to the servers are ciphertexts encrypted within the same public-key encryption scheme. These Mix-Nets rely on homomorphic operations that allow the servers to re-randomize ciphertexts, thereby re-encrypting the corresponding plaintexts.
- `Remark` [^8]: An electronic exam protocol and meant to guarantee anonymity of both candidates and examiners without relying on trusted parties.

## License

GNU General Public License v3[^3].

## Prerequisites

This project uses the following:
* ProVerif[^9] version 2.05.

All our files are publicly available and can be accessed online through this Gitlab repository. The repository also contains an installation script for Ubuntu Linux, which has been successfully tested on a fresh Ubuntu VM 24.04 LTS, installed from the official Ubuntu ISO image[^11]. On other operating systems, refer to the ProVerif website[^9] for installation instructions.

## Access through Gitlab and software installation

```
git clone https://gitlab.limos.fr/dhmahmoud/usenix24-632
cd usenix24-632
sh run_install-dep-pv.sh
```

## Basic tests

After running the installation script, perform a basic test using the following commands:

* Test for ProVerif
```
./proverif2.05/proverif --help

```
In the beginning you should see the following:
```
Proverif 2.05. Cryptographic protocol verifier, by Bruno Blanchet, Vincent Cheval, and Marc Sylvestre
```

#### Running the files
The command below can be used to check a security property. Each security property can be tested in the presence or absence of zero knowledge proofs.

```
./proverif2.05/proverif <protocol>_<property>_<option>.pv

```
where `<property>` can be `Anonymous-Shuffling`, `Anonymous-Examiner`, `Anonymous-Marking`, and `Vote-Privacy`; `<option>` is either `Without-ZKP`, `Weak-ZKP` or `Strong-ZKP`.


## Results and Performances

To test on one of the fastest model please use the following script that takes few seconds:

```
time  ./proverif2.05/proverif Exp-MixNets_Anonymous-Shuffling_Without-ZKP.pv
```

The following script runs all our models one by one and provides after each result given by ProVerif the execution time.

```
sh run-all.sh
```

Table below provides the result and execution time (on a standard laptop) of each ProVerif file contained in this repository (also corresponds to Table 4 and 5 of the paper).


| ProVerif File     		    	      	       	 	|       Results   		| Time    |
|---    						|:-:    				|:-:      |
| Crypto_Santa-Anonymous-Shuffling_Weak-ZKP			|	Cannot be proved	| 4m06s	  |
| Crypto_Santa-Anonymous-Shuffling_Strong-ZKP			|	True			|    9s	  |
| Exp-MixNets_Anonymous-Shuffling_Without-ZKP			|	Cannot be proved	|    2s	  |
| Exp-MixNets_Anonymous-Shuffling_Weak-ZKP	 		|	Cannot be proved	| 1m06s   |
| Exp-MixNets_Anonymous-Shuffling_Strong-ZKP	 		|	True			|    3s	  |
| Haenni-Protocol_Vote-Privacy_Without-ZKP	 		|	Cannot be proved	| 4m35s   |
| Haenni-Protocol_Vote-Privacy_Weak-ZKP	 			|	Cannot be proved	| 9m35s   |
| Haenni-Protocol_Vote-Privacy_Strong-ZKP	 		|	True			|   14s	  |
| IVXV-Protocol_Vote-Privacy_Without-ZKP	 		|	Cannot be proved	|    1s	  |
| IVXV-Protocol_Vote-Privacy_Weak-ZKP	 			|	Cannot be proved	|   25s	  |
| IVXV-Protocol_Vote-Privacy_Strong-ZKP	 			|	True			|    8s	  |
| ReEncryption-MixNets_Anonymous-Shuffling_Without-ZKP	 	|	Cannot be proved	|    1s	  |
| ReEncryption-MixNets_Anonymous-Shuffling_Weak-ZKP	 	|	Cannot be proved	|    2s	  |
| ReEncryption-MixNets_Anonymous-Shuffling_Strong-ZKP	 	|	True			|    1s	  |
| Remark_Anonymous-Examiner_Without-ZKP	 			|	Cannot be proved	| 4m19s   |
| Remark_Anonymous-Examiner_Weak-ZKP	 			|	Cannot be proved	| 9m23s	  |
| Remark_Anonymous-Examiner_Strong-ZKP	 			|	True			|    7s	  |
| Remark_Anonymous-Marking_Without-ZKP	 			|	Cannot be proved	| 3m16s	  |
| Remark_Anonymous-Marking_Weak-ZKP	 			|	Cannot be proved	| 9m35s   |
| Remark_Anonymous-Marking_Strong-ZKP	 			|	True			|   11s	  |


## References

[^0]: https://members.loria.fr/JDreier/
[^1]: https://sancy.iut.uca.fr/~lafourcade/
[^2]: https://perso.isima.fr/~dhmahmoud/
[^3]: https://www.gnu.org/licenses/gpl-3.0.html
[^4]: "Peter Y. Ryan. Crypto santa. In LNCS Essays on The New Codebreakers - Volume 9100, page 543–549, Berlin, Heidelberg, 2015. Springer-Verlag"
[^5]: "Rolf Haenni and Oliver Spycher. Secure internet voting on limited devices with anonymized DSA public keys. In 2011 Electronic Voting Technology Workshop on Trustworthy Elections (EVT/WOTE 11), San Francisco, CA, August 2011. USENIX Association"
[^6]: https://www.valimised.ee/sites/default/files/2023-02/IVXV-protocols.pdf
[^7]: "Choonsik Park, Kazutomo Itoh, and Kaoru Kurosawa. Efficient anonymous channel and all/nothing election scheme. In Advances in Cryptology - EUROCRYPT 93, Workshop on the Theory and Application of of Cryptographic Techniques, Lofthus, Norway, May 23-27, 1993, Proceedings, volume 765 of Lecture Notes in Computer Science, pages 248–259. Springer, 1993"
[^8]: "Rosario Giustolisi, Gabriele Lenzini, and Peter Y. A. Ryan. Remark!: A secure protocol for remote exams. In Security Protocols Workshop, 2014"
[^9]: https://bblanche.gitlabpages.inria.fr/proverif/
[^10]: "Jannik Dreier, Pascal Lafourcade and Dhekra Mahmoud, Shaken, not Stirred - Automated Discovery of Subtle Attacks on Protocols using Mix-Nets. In The
33rd USENIX Security Symposium 2024"
[^11]: https://www.ubuntu-fr.org/download/
